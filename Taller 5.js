//  Ejercicio 1
/*  Cree un array llamado meses, este array deberá almacenar el nombre de los doce meses del año. 
    Muestre por pantalla el nombre de cada uno de ellos utilizando un bucle for. */

var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
             "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

var tamañoArray = meses.length;

console.log("Meses del año:")
for(i = 0; i < tamañoArray; i++){
  console.log("- " + meses[i]);
}

//  Ejercicio 2
/*  Cree un script que defina un objeto llamado Producto_alimenticio, este objeto debe presentar 
    las propiedades código, nombre y precio, además del método imprimeDatos, el cual escribe por 
    pantalla los valores de sus propiedades. Posteriormente, cree tres instancias de este objeto y 
    guárdelas en un array. Con la ayuda del bucle for, utilice el método imprimeDatos para mostrar 
    por pantalla los valores de los tres objetos instanciados. */

var Producto_alimenticio = {
  codigo: '000',
  nombre: 'prueba',
  precio: '0.00',
  imprimeDatos: function() {
    console.log("Código: " + this.codigo + "\n" +
                "Nombre: " + this.nombre + "\n" +
                "Precio: " + this.precio
    );
  }
};
  
var producto1 = Object.create(Producto_alimenticio)
producto1.codigo = "0001";
producto1.nombre = "Queso fresco";
producto1.precio = "4,50";
  
var producto2 = Object.create(Producto_alimenticio)
producto2.codigo = "0002";
producto2.nombre = "Arroz (1kg)";
producto2.precio = "1,33";
  
var producto3 = Object.create(Producto_alimenticio)
producto3.codigo = "0003";
producto3.nombre = "Patatas (1 kg)";
producto3.precio = "1,06";
  
var misProductos = [producto1, producto2, producto3]; //Array
  
for(i = 0; i < 3; i++){
  console.log("-------" + "\n" +
              "Producto " + (i+1) + ":");
  if (i == 0) {
    producto1.imprimeDatos();
  } else if (i == 1) {
    producto2.imprimeDatos();
  } else {
    producto3.imprimeDatos();
  }
}